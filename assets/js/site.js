$( function() {
	var $container = $('#masonry').isotope({
		layout: masonry,
	});
	// filter items on button click
	$('#filters').on( 'click', 'a.button', function() {
		var filterValue = $(this).attr('data-filter');
		$container.isotope({ filter: filterValue });
	});

	$(".stellar").stellar({
		horizontalScrolling: true,
		responsive: true,
		verticalOffset: 40
	});

});